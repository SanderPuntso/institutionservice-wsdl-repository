
package ee.ttu.webservice.institution;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetInstitutionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetInstitutionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="info" type="{http://www.ttu.ee/webservice/institution}InstitutionType"/&gt;
 *         &lt;element name="groupList" type="{http://www.ttu.ee/webservice/institution}GroupListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetInstitutionType", propOrder = {
    "info",
    "groupList"
})
public class GetInstitutionType {

    @XmlElement(required = true)
    protected InstitutionType info;
    @XmlElement(required = true)
    protected GroupListType groupList;

    /**
     * Gets the value of the info property.
     * 
     * @return
     *     possible object is
     *     {@link InstitutionType }
     *     
     */
    public InstitutionType getInfo() {
        return info;
    }

    /**
     * Sets the value of the info property.
     * 
     * @param value
     *     allowed object is
     *     {@link InstitutionType }
     *     
     */
    public void setInfo(InstitutionType value) {
        this.info = value;
    }

    /**
     * Gets the value of the groupList property.
     * 
     * @return
     *     possible object is
     *     {@link GroupListType }
     *     
     */
    public GroupListType getGroupList() {
        return groupList;
    }

    /**
     * Sets the value of the groupList property.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupListType }
     *     
     */
    public void setGroupList(GroupListType value) {
        this.groupList = value;
    }

}
