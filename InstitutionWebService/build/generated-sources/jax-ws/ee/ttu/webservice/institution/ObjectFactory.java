
package ee.ttu.webservice.institution;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ee.ttu.webservice.institution package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ee.ttu.webservice.institution
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetTokenResponse }
     * 
     */
    public GetTokenResponse createGetTokenResponse() {
        return new GetTokenResponse();
    }

    /**
     * Create an instance of {@link GetTokenRequest }
     * 
     */
    public GetTokenRequest createGetTokenRequest() {
        return new GetTokenRequest();
    }

    /**
     * Create an instance of {@link GetTokenResponse.UserToken }
     * 
     */
    public GetTokenResponse.UserToken createGetTokenResponseUserToken() {
        return new GetTokenResponse.UserToken();
    }

    /**
     * Create an instance of {@link ErrorType }
     * 
     */
    public ErrorType createErrorType() {
        return new ErrorType();
    }

    /**
     * Create an instance of {@link AddUserRequest }
     * 
     */
    public AddUserRequest createAddUserRequest() {
        return new AddUserRequest();
    }

    /**
     * Create an instance of {@link UserType }
     * 
     */
    public UserType createUserType() {
        return new UserType();
    }

    /**
     * Create an instance of {@link AddUserResponse }
     * 
     */
    public AddUserResponse createAddUserResponse() {
        return new AddUserResponse();
    }

    /**
     * Create an instance of {@link GetUserType }
     * 
     */
    public GetUserType createGetUserType() {
        return new GetUserType();
    }

    /**
     * Create an instance of {@link AddInstitutionRequest }
     * 
     */
    public AddInstitutionRequest createAddInstitutionRequest() {
        return new AddInstitutionRequest();
    }

    /**
     * Create an instance of {@link InstitutionType }
     * 
     */
    public InstitutionType createInstitutionType() {
        return new InstitutionType();
    }

    /**
     * Create an instance of {@link AddInstitutionResponse }
     * 
     */
    public AddInstitutionResponse createAddInstitutionResponse() {
        return new AddInstitutionResponse();
    }

    /**
     * Create an instance of {@link AddGroupRequest }
     * 
     */
    public AddGroupRequest createAddGroupRequest() {
        return new AddGroupRequest();
    }

    /**
     * Create an instance of {@link GroupType }
     * 
     */
    public GroupType createGroupType() {
        return new GroupType();
    }

    /**
     * Create an instance of {@link AddGroupResponse }
     * 
     */
    public AddGroupResponse createAddGroupResponse() {
        return new AddGroupResponse();
    }

    /**
     * Create an instance of {@link GetGroupType }
     * 
     */
    public GetGroupType createGetGroupType() {
        return new GetGroupType();
    }

    /**
     * Create an instance of {@link AddInstitutionManagerRequest }
     * 
     */
    public AddInstitutionManagerRequest createAddInstitutionManagerRequest() {
        return new AddInstitutionManagerRequest();
    }

    /**
     * Create an instance of {@link InstitutionMemberType }
     * 
     */
    public InstitutionMemberType createInstitutionMemberType() {
        return new InstitutionMemberType();
    }

    /**
     * Create an instance of {@link AddInstitutionManagerResponse }
     * 
     */
    public AddInstitutionManagerResponse createAddInstitutionManagerResponse() {
        return new AddInstitutionManagerResponse();
    }

    /**
     * Create an instance of {@link AddInstitutionMemberRequest }
     * 
     */
    public AddInstitutionMemberRequest createAddInstitutionMemberRequest() {
        return new AddInstitutionMemberRequest();
    }

    /**
     * Create an instance of {@link AddInstitutionMemberResponse }
     * 
     */
    public AddInstitutionMemberResponse createAddInstitutionMemberResponse() {
        return new AddInstitutionMemberResponse();
    }

    /**
     * Create an instance of {@link AddGroupManagerRequest }
     * 
     */
    public AddGroupManagerRequest createAddGroupManagerRequest() {
        return new AddGroupManagerRequest();
    }

    /**
     * Create an instance of {@link GroupMemberType }
     * 
     */
    public GroupMemberType createGroupMemberType() {
        return new GroupMemberType();
    }

    /**
     * Create an instance of {@link AddGroupManagerResponse }
     * 
     */
    public AddGroupManagerResponse createAddGroupManagerResponse() {
        return new AddGroupManagerResponse();
    }

    /**
     * Create an instance of {@link AddGroupMemberRequest }
     * 
     */
    public AddGroupMemberRequest createAddGroupMemberRequest() {
        return new AddGroupMemberRequest();
    }

    /**
     * Create an instance of {@link AddGroupMemberResponse }
     * 
     */
    public AddGroupMemberResponse createAddGroupMemberResponse() {
        return new AddGroupMemberResponse();
    }

    /**
     * Create an instance of {@link GetUserRequest }
     * 
     */
    public GetUserRequest createGetUserRequest() {
        return new GetUserRequest();
    }

    /**
     * Create an instance of {@link GetUserResponse }
     * 
     */
    public GetUserResponse createGetUserResponse() {
        return new GetUserResponse();
    }

    /**
     * Create an instance of {@link UserListType }
     * 
     */
    public UserListType createUserListType() {
        return new UserListType();
    }

    /**
     * Create an instance of {@link GetInstitutionRequest }
     * 
     */
    public GetInstitutionRequest createGetInstitutionRequest() {
        return new GetInstitutionRequest();
    }

    /**
     * Create an instance of {@link GetInstitutionResponse }
     * 
     */
    public GetInstitutionResponse createGetInstitutionResponse() {
        return new GetInstitutionResponse();
    }

    /**
     * Create an instance of {@link InstitutionListType }
     * 
     */
    public InstitutionListType createInstitutionListType() {
        return new InstitutionListType();
    }

    /**
     * Create an instance of {@link GetInstitutionManagerRequest }
     * 
     */
    public GetInstitutionManagerRequest createGetInstitutionManagerRequest() {
        return new GetInstitutionManagerRequest();
    }

    /**
     * Create an instance of {@link GetInstitutionManagerResponse }
     * 
     */
    public GetInstitutionManagerResponse createGetInstitutionManagerResponse() {
        return new GetInstitutionManagerResponse();
    }

    /**
     * Create an instance of {@link GetInstitutionMemberRequest }
     * 
     */
    public GetInstitutionMemberRequest createGetInstitutionMemberRequest() {
        return new GetInstitutionMemberRequest();
    }

    /**
     * Create an instance of {@link GetInstitutionMemberResponse }
     * 
     */
    public GetInstitutionMemberResponse createGetInstitutionMemberResponse() {
        return new GetInstitutionMemberResponse();
    }

    /**
     * Create an instance of {@link GetGroupRequest }
     * 
     */
    public GetGroupRequest createGetGroupRequest() {
        return new GetGroupRequest();
    }

    /**
     * Create an instance of {@link GetGroupResponse }
     * 
     */
    public GetGroupResponse createGetGroupResponse() {
        return new GetGroupResponse();
    }

    /**
     * Create an instance of {@link GroupListType }
     * 
     */
    public GroupListType createGroupListType() {
        return new GroupListType();
    }

    /**
     * Create an instance of {@link GetGroupManagerRequest }
     * 
     */
    public GetGroupManagerRequest createGetGroupManagerRequest() {
        return new GetGroupManagerRequest();
    }

    /**
     * Create an instance of {@link GetGroupManagerResponse }
     * 
     */
    public GetGroupManagerResponse createGetGroupManagerResponse() {
        return new GetGroupManagerResponse();
    }

    /**
     * Create an instance of {@link GetGroupMemberRequest }
     * 
     */
    public GetGroupMemberRequest createGetGroupMemberRequest() {
        return new GetGroupMemberRequest();
    }

    /**
     * Create an instance of {@link GetGroupMemberResponse }
     * 
     */
    public GetGroupMemberResponse createGetGroupMemberResponse() {
        return new GetGroupMemberResponse();
    }

    /**
     * Create an instance of {@link DeleteUserRequest }
     * 
     */
    public DeleteUserRequest createDeleteUserRequest() {
        return new DeleteUserRequest();
    }

    /**
     * Create an instance of {@link DeleteUserResponse }
     * 
     */
    public DeleteUserResponse createDeleteUserResponse() {
        return new DeleteUserResponse();
    }

    /**
     * Create an instance of {@link DeleteInstitutionRequest }
     * 
     */
    public DeleteInstitutionRequest createDeleteInstitutionRequest() {
        return new DeleteInstitutionRequest();
    }

    /**
     * Create an instance of {@link DeleteInstitutionResponse }
     * 
     */
    public DeleteInstitutionResponse createDeleteInstitutionResponse() {
        return new DeleteInstitutionResponse();
    }

    /**
     * Create an instance of {@link DeleteGroupRequest }
     * 
     */
    public DeleteGroupRequest createDeleteGroupRequest() {
        return new DeleteGroupRequest();
    }

    /**
     * Create an instance of {@link DeleteGroupResponse }
     * 
     */
    public DeleteGroupResponse createDeleteGroupResponse() {
        return new DeleteGroupResponse();
    }

    /**
     * Create an instance of {@link DeleteInstitutionManagerRequest }
     * 
     */
    public DeleteInstitutionManagerRequest createDeleteInstitutionManagerRequest() {
        return new DeleteInstitutionManagerRequest();
    }

    /**
     * Create an instance of {@link DeleteInstitutionManagerResponse }
     * 
     */
    public DeleteInstitutionManagerResponse createDeleteInstitutionManagerResponse() {
        return new DeleteInstitutionManagerResponse();
    }

    /**
     * Create an instance of {@link DeleteInstitutionMemberRequest }
     * 
     */
    public DeleteInstitutionMemberRequest createDeleteInstitutionMemberRequest() {
        return new DeleteInstitutionMemberRequest();
    }

    /**
     * Create an instance of {@link DeleteInstitutionMemberResponse }
     * 
     */
    public DeleteInstitutionMemberResponse createDeleteInstitutionMemberResponse() {
        return new DeleteInstitutionMemberResponse();
    }

    /**
     * Create an instance of {@link DeleteGroupManagerRequest }
     * 
     */
    public DeleteGroupManagerRequest createDeleteGroupManagerRequest() {
        return new DeleteGroupManagerRequest();
    }

    /**
     * Create an instance of {@link DeleteGroupManagerResponse }
     * 
     */
    public DeleteGroupManagerResponse createDeleteGroupManagerResponse() {
        return new DeleteGroupManagerResponse();
    }

    /**
     * Create an instance of {@link DeleteGroupMemberRequest }
     * 
     */
    public DeleteGroupMemberRequest createDeleteGroupMemberRequest() {
        return new DeleteGroupMemberRequest();
    }

    /**
     * Create an instance of {@link DeleteGroupMemberResponse }
     * 
     */
    public DeleteGroupMemberResponse createDeleteGroupMemberResponse() {
        return new DeleteGroupMemberResponse();
    }

    /**
     * Create an instance of {@link UpdateUserRequest }
     * 
     */
    public UpdateUserRequest createUpdateUserRequest() {
        return new UpdateUserRequest();
    }

    /**
     * Create an instance of {@link UpdateUserResponse }
     * 
     */
    public UpdateUserResponse createUpdateUserResponse() {
        return new UpdateUserResponse();
    }

    /**
     * Create an instance of {@link UpdateInstitutionRequest }
     * 
     */
    public UpdateInstitutionRequest createUpdateInstitutionRequest() {
        return new UpdateInstitutionRequest();
    }

    /**
     * Create an instance of {@link UpdateInstitutionResponse }
     * 
     */
    public UpdateInstitutionResponse createUpdateInstitutionResponse() {
        return new UpdateInstitutionResponse();
    }

    /**
     * Create an instance of {@link UpdateGroupRequest }
     * 
     */
    public UpdateGroupRequest createUpdateGroupRequest() {
        return new UpdateGroupRequest();
    }

    /**
     * Create an instance of {@link UpdateGroupResponse }
     * 
     */
    public UpdateGroupResponse createUpdateGroupResponse() {
        return new UpdateGroupResponse();
    }

    /**
     * Create an instance of {@link GetInstitutionType }
     * 
     */
    public GetInstitutionType createGetInstitutionType() {
        return new GetInstitutionType();
    }

    /**
     * Create an instance of {@link InstitutionGroupType }
     * 
     */
    public InstitutionGroupType createInstitutionGroupType() {
        return new InstitutionGroupType();
    }

}
