/**
 * 
 *     Service: InstitutionService
 *     Version: 1.0
 *     Owner: Sander Puntso
 *   
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://www.ttu.ee/webservice/institution", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package ee.ttu.webservice.institution;
