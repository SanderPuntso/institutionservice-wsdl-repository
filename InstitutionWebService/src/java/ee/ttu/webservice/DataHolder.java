/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.webservice;

import ee.ttu.webservice.institution.*;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author sande
 */
public class DataHolder {
    static List<LoginToken> tokens = new ArrayList<LoginToken>();
    static List<GetUserType> users = new ArrayList<GetUserType>();
    static List<InstitutionType> institutions = new ArrayList<InstitutionType>();
    static List<InstitutionMemberType> institutionMembers = new ArrayList<InstitutionMemberType>();
    static List<InstitutionMemberType> institutionManagers = new ArrayList<InstitutionMemberType>();
    static List<GetGroupType> groups = new ArrayList<GetGroupType>();
    static List<GroupMemberType> groupMembers= new ArrayList<GroupMemberType>();
    static List<GroupMemberType> groupManagers= new ArrayList<GroupMemberType>();
    static List<InstitutionGroupType> institutionGroups = new ArrayList<InstitutionGroupType>();
    
    public DataHolder() {
        
    }
    
}
