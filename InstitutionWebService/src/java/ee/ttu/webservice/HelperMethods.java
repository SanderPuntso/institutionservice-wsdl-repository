/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.webservice;

import static ee.ttu.webservice.DataHolder.*;
import ee.ttu.webservice.institution.*;
import ee.ttu.webservice.institution.GetTokenResponse.*;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author sande
 */
public class HelperMethods {

    public HelperMethods() {
    }

    Map<String, String> errorList = new HashMap<String, String>() {{
        put("400xUaErr","Process with token unauthorized!");
    }};
    public UserToken getUserTokenByLoginInfo(String username, String password){
        UserToken response;
        response = null;
        for(LoginToken token:tokens){
            if(token.getUserName().equalsIgnoreCase(username) && token.getPassWord().equalsIgnoreCase(password)){
                for(GetUserType use:users){
                    if(use.getId().equals(token.getUserId())){
                        UserToken foundUser = new UserToken();
                        foundUser.setUser(use);
                        foundUser.setToken(token.getToken()); 
                        response = foundUser;
                    }
                }
            }
        }
        return response;
    }
    public BigInteger getUserIdByToken(String token){
        BigInteger response = null;
        for(LoginToken user:tokens){
            if(user.getToken().equalsIgnoreCase(token)){
                response = user.getUserId();
            }
        }
        return response;
    }
    public boolean usernameInUse(String username){
        boolean response = false;
        for(LoginToken user:tokens){
            if(user.getUserName().equalsIgnoreCase(username)){
                response = true;
            }
        }
        return response;
    }
    public boolean getTokenAuthorized(String token){
        boolean response = false;
        for(LoginToken user:tokens){
            if(user.getToken().equalsIgnoreCase(token)){
                response = true;
            }
        }
        return response;
    }
    public GetUserType getUserById(String id){
        GetUserType response = null;
        for(GetUserType user:users){
            if(user.getIdcode().equalsIgnoreCase(id)){
                response = user;
            }
        }
        return response;
    }
    public GetUserType getUserByUserId(BigInteger id){
        GetUserType response = null;
        for(GetUserType user:users){
            if(user.getId().equals(id)){
                response = user;
            }
        }
        return response;
    }
    public InstitutionType getInstitutionByRegistryCode(String registryCode){
        InstitutionType response = null;
        for(InstitutionType institution:institutions){
            if(institution.getRegistryCode().equalsIgnoreCase(registryCode)){
                response = institution;
            }
        }
        return response;
    }
    public boolean validateInstitutionManager(String token, String registryCode){
        boolean response = false;
        BigInteger userId = getUserIdByToken(token);
        for(InstitutionMemberType manager:institutionManagers){
            if(manager.getRegistryCode().equals(registryCode) && manager.getId().equals(userId)){
                response = true;
            }
        }
        return response;
    }
    public GetGroupType getGroupById(String id){
        GetGroupType response = null;
        for(GetGroupType group:groups){
            if(group.getId().equals(id)){
                response = group;
            }
        }
        return response;
    }
    public LoginToken getLoginTokenById(String id){
        LoginToken response = null;
        for(LoginToken token:tokens){
            if(token.getUserId().equals(id)){
                response = token;
            }
        }
        return response;
    }

    public boolean getGroupRegistred(String name, String registryCode) {
        boolean response = false;
        for(InstitutionGroupType institution:institutionGroups){
            if(institution.getRegistryCode().equalsIgnoreCase(registryCode)){
                for(GetGroupType group:groups){
                    if(institution.getGroupId().equals(group.getId()) && group.getName().equals(name)){
                        response = true;
                    }
                }
            }
        }
        return response;
    }

    boolean authorizeInstitutionManager1(String token, String registryCode) {
        boolean response = false;
        for(InstitutionMemberType institution:institutionManagers){
            if(institution.getRegistryCode().equals(registryCode) && institution.getId().equals(getUserIdByToken(token))){
                response = true;
            }
        }
        return response;
    }

    boolean getAlreadyManager(InstitutionMemberType manager) {
        boolean response = false;
        for(InstitutionMemberType man:institutionManagers){
            if(man.getId().equals(manager.getId()) && man.getRegistryCode().equals(manager.getRegistryCode())){
                response = true;
            }
        }
        return response;
    }

    boolean getAlreadyMember(InstitutionMemberType member) {
        boolean response = false;
        for(InstitutionMemberType man:institutionMembers){
            if(man.getId().equals(member.getId()) && man.getRegistryCode().equals(member.getRegistryCode())){
                response = true;
            }
        }
        return response;
    }

    boolean checkUsernameFree(String username){
        boolean response = true;
        for(LoginToken token:tokens){
            if(token.getUserName().equalsIgnoreCase(username)){
                response = false;
            }
        }
        return response;
    }
    boolean checkIdCodeFree(String idCode){
        boolean response = true;
        for(GetUserType user:users){
            if(user.getIdcode().equalsIgnoreCase(idCode)){
                response = false;
            }
        }
        return response;
    }
    boolean checkInstitutionRegistryCodeFree(String registryCode){
        boolean response = true;
        for(InstitutionType institution:institutions){
            if(institution.getRegistryCode().equalsIgnoreCase(registryCode)){
                response = false;
            }
        }
        return response;
    }
    boolean checkGroupUnregistred(String registryCode, String name){
        boolean response = true;
        for(InstitutionType institution:institutions){
            if(institution.getRegistryCode().equalsIgnoreCase(registryCode)){
                for(InstitutionGroupType group:institutionGroups){
                    if(group.getRegistryCode().equalsIgnoreCase(registryCode)){
                        for(GetGroupType groupType:groups){
                            if(groupType.getName().equals(name)){
                                response = false;
                            }
                        }
                    }
                    
                }
            }
        }
        return response;
    }
    boolean authorizeUserRequest(String token, BigInteger userId){
        boolean response = false;
        GetUserType accessingUser = getUserWithToken(token);
        GetUserType accessableUser = getUserWithId(userId);
        for(InstitutionMemberType member:institutionMembers){
            if(member.getId().equals(accessableUser.getId()) && authorizeInstitutionManager(token, member.getRegistryCode())){
                response = true;
            } else if (member.getId().equals(accessableUser.getId()) && authorizeInstitutionMember(token, member.getRegistryCode())){
                response = true;
            }
        }
        for(GroupMemberType member:groupMembers){
            if(member.getId().equals(accessableUser.getId()) && authorizeGroupManager(token, member.getGroupId())){
                response = true;
            }
            for(InstitutionGroupType group:institutionGroups){
                if(member.getGroupId().equals(group.getGroupId()) && authorizeInstitutionManager(token, group.getRegistryCode())){
                    response = true;
                } else if(member.getGroupId().equals(group.getGroupId()) && authorizeInstitutionMember(token, group.getRegistryCode())){
                    response = true;
                }
            }
        }
        return response;
    }
    boolean authorizeGroupManager(String token, BigInteger groupId){
        boolean response = false;
        for(GroupMemberType manager:groupManagers){
            if(manager.getGroupId().equals(groupId) && manager.getId().equals(getUserWithToken(token).getId())){
                response = true;
            }
        }
        return response;
    }
    boolean authorizeGroupManager(BigInteger userId, BigInteger groupId){
        boolean response = false;
        for(GroupMemberType manager:groupManagers){
            if(manager.getGroupId().equals(groupId) && manager.getId().equals(userId)){
                response = true;
            }
        }
        return response;
    }
    boolean authorizeGroupMember(String token, BigInteger groupId){
        boolean response = false;
        for(GroupMemberType member:groupMembers){
            if(member.getGroupId().equals(groupId) && member.getId().equals(getUserWithToken(token).getId())){
                response = true;
            }
        }
        return response;
    }
    boolean authorizeGroupMember(BigInteger userId, BigInteger groupId){
        boolean response = false;
        for(GroupMemberType member:groupMembers){
            if(member.getGroupId().equals(groupId) && member.getId().equals(userId)){
                response = true;
            }
        }
        return response;
    }
    boolean authorizeInstitutionManager(String token, String registryCode){
        boolean response = false;
        for(InstitutionMemberType manager:institutionManagers){
            if(manager.getRegistryCode().equals(registryCode) && manager.getId().equals(getUserWithToken(token).getId())){
                response = true;
            }
        }
        return response;
    }
    boolean authorizeInstitutionManager(BigInteger userId, String registryCode){
        boolean response = false;
        for(InstitutionMemberType manager:institutionManagers){
            if(manager.getRegistryCode().equals(registryCode) && manager.getId().equals(userId)){
                response = true;
            }
        }
        return response;
    }
    boolean authorizeInstitutionMember(String token, String registryCode){
        boolean response = false;
        for(InstitutionMemberType member:institutionMembers){
            if(member.getRegistryCode().equals(registryCode) && member.getId().equals(getUserWithToken(token).getId())){
                response = true;
            }
                
        }
        return response;
    }
    boolean authorizeInstitutionMember(BigInteger userId, String registryCode){
        boolean response = false;
        for(InstitutionMemberType member:institutionMembers){
            if(member.getRegistryCode().equalsIgnoreCase(registryCode) && member.getId().equals(userId)){
                response = true;
            }
        }
        return response;
    }
    boolean authorizeToken(String token){
        boolean response = false;
        for(LoginToken loginToken:tokens){
            if(loginToken.getToken().equalsIgnoreCase(token)){
                response = true;
            }
        }
        return response;
    }
    GetUserType getUserWithId(BigInteger id){
        GetUserType response = null;
        for(GetUserType user:users){
            if(user.getId().equals(id)){
                response = user;
            }
        }
        return response;
    }
    GetUserType getUserWithToken(String token){
        GetUserType response;
        BigInteger userId = null;
        for(LoginToken loginToken:tokens){
            if(loginToken.getToken().equalsIgnoreCase(token)){
                userId = loginToken.getUserId();
            }
        }
        response = getUserWithId(userId);
        return response;
    }
    InstitutionType getInstitutionWithGroupId(BigInteger id){
        InstitutionType response = null;
        for(InstitutionGroupType group:institutionGroups){
            if(group.getGroupId().equals(id)){
                response = getInstitutionByRegistryCode(group.getRegistryCode());
            }
        }
        return response;
    }
    GetGroupType getGroupWithId(BigInteger id){
        GetGroupType response = null;
        for(GetGroupType group:groups){
            if(group.getId().equals(id)){
                response = group;
            }
        }
        return response;
    }

    boolean checkOnlyManager(String registryCode) {
        Integer managerCount = 0;
        boolean response = true;
        for(InstitutionMemberType manager:institutionManagers){
            if(manager.getRegistryCode().equalsIgnoreCase(registryCode)){
                managerCount++;
            }
        }
        if(managerCount>1){
            response = false;
        }
        return response;
    }

    boolean authorizeGetUser(String token, BigInteger id) {
        boolean response = false;
        for(InstitutionType institution:institutions){
            if(authorizeInstitutionManager(token, institution.getRegistryCode()) || authorizeInstitutionMember(token, institution.getRegistryCode())){
                for(InstitutionMemberType member:institutionMembers){
                    if(member.getId().equals(id)){
                        response = true;
                    }
                }
                for(InstitutionMemberType manager:institutionManagers){
                    if(manager.getId().equals(id)){
                        response = true;
                    }
                }
                for(GroupMemberType manager:groupManagers){
                    if(getInstitutionWithGroupId(manager.getGroupId()).getRegistryCode().equalsIgnoreCase(institution.getRegistryCode()) &&
                        manager.getId().equals(id)){
                        response = true;
                    }
                }
                for(GroupMemberType member:groupMembers){
                    if(getInstitutionWithGroupId(member.getGroupId()).getRegistryCode().equalsIgnoreCase(institution.getRegistryCode()) &&
                        member.getId().equals(id)){
                        response = true;
                    }
                }
            }
        }
        return response;
    }

    boolean authorizeUserDeletion(BigInteger id) {
        boolean response = true;
        for(InstitutionMemberType manager:institutionManagers){
            if(manager.getId().equals(id)){
                Integer count = 0;
                for(InstitutionMemberType anotherManager:institutionManagers){
                    if(!anotherManager.getId().equals(id) && anotherManager.getRegistryCode().equals(manager.getRegistryCode())){
                        count++;
                    }
                }
                if(count == 0){
                    response = false;
                }
            }
        }
        return response;
    }
}
