/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.webservice;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sande
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoginToken", propOrder = {
    "userName",
    "passWord",
    "token",
    "userId"
})
public class LoginToken {
    @XmlElement(required = true)
    protected String userName;
    @XmlElement(required = true)
    protected String passWord;
    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    protected BigInteger userId;

    public LoginToken() {}

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public BigInteger getUserId() {
        return userId;
    }

    public void setUserId(BigInteger idCode) {
        this.userId = idCode;
    }
}