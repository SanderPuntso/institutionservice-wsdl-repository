/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.webservice;

import ee.ttu.webservice.institution.*;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Sander Puntso
 */
@Path("user")
public class UserResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of UserResource
     */
    public UserResource() {
    }    
    
    /**
    *USER GET/POST/UPDATE/DELETE
    */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public GetUserResponse getUserList(@QueryParam("token") String token) {
        //TODO return proper representation object
        InstitutionService ws = new InstitutionService();
        GetUserRequest request = new GetUserRequest();
        request.setToken(token);
        return ws.getUser(request);
    }
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public GetUserResponse getUser(@PathParam("id") BigInteger id, @QueryParam("token") String token) {
        //TODO return proper representation object
        InstitutionService ws = new InstitutionService();
        GetUserRequest request = new GetUserRequest();
        request.setToken(token);
        request.setId(id);
        return ws.getUser(request);
    }
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public AddUserResponse addUser(AddUserRequest content) {
        //TODO return proper representation object
        InstitutionService ws = new InstitutionService();
        AddUserRequest request = new AddUserRequest();
        request.setUser(content.getUser());
        request.setUsername(content.getUsername());
        request.setPassword(content.getPassword());
        return ws.addUser(request);
    }
    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UpdateUserResponse updateUser(UserType content, 
            @PathParam("id") BigInteger id, @QueryParam("token") String token) {
        InstitutionService ws = new InstitutionService();
        UpdateUserRequest request = new UpdateUserRequest();
        request.setToken(token);
        request.setId(id);
        request.setUser(content);
        return ws.updateUser(request);
    }
    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public DeleteUserResponse deleteUser(@PathParam("id") BigInteger id, @QueryParam("token") String token) {
    InstitutionService ws = new InstitutionService();
        //TODO return proper representation object
        DeleteUserRequest request = new DeleteUserRequest();
        request.setToken(token);
        request.setId(id);
        return ws.deleteUser(request);
    }

    /**
     * TOKEN GET
     */
    @GET
    @Path("/token")
    @Produces(MediaType.APPLICATION_JSON)
    public GetTokenResponse getToken(@QueryParam("username") String username, @QueryParam("password") String password) {
    InstitutionService ws = new InstitutionService();
        GetTokenRequest request = new GetTokenRequest();
        request.setUsername(username);
        request.setPassword(password);
        return ws.getToken(request);
    }
}
