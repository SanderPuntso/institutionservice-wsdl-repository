/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.webservice;

import com.sun.xml.ws.developer.SchemaValidation;
import static ee.ttu.webservice.DataHolder.*;
import ee.ttu.webservice.institution.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.jws.WebService;
import javax.ws.rs.WebApplicationException;

/**
 *
 * @author Sander Puntso
 */
@SchemaValidation
@WebService(serviceName = "InstitutionService", portName = "InstitutionPort", endpointInterface = "ee.ttu.webservice.institution.InstitutionPortType", targetNamespace = "http://www.ttu.ee/webservice/institution", wsdlLocation = "WEB-INF/wsdl/InstitutionService/InstitutionService.wsdl")
public class InstitutionService {
    HelperMethods methodStack = new HelperMethods();
    ErrorType error = new ErrorType();
    private static int nextUserId = 100;
    private static int nextGroupId = 100;
    public GetTokenResponse getToken(GetTokenRequest parameter) {
        //DONE
        GetTokenResponse response = new GetTokenResponse();
        try {
            if(!(methodStack.getUserTokenByLoginInfo((String) parameter.getUsername(), parameter.getPassword())==null)){
                GetTokenResponse.UserToken token;
                token = methodStack.getUserTokenByLoginInfo(parameter.getUsername(), parameter.getPassword());
                response.setSuccess(SuccessType.TRUE);
                response.setUserToken(token); 
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UsrPwdErr");
                error.setErrorMessage("Username or password is incorrect!");
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public AddUserResponse addUser(AddUserRequest parameter) {
        //DONE
        AddUserResponse response = new AddUserResponse();
        try {
            if(methodStack.checkUsernameFree(parameter.getUsername()) && methodStack.checkIdCodeFree(parameter.getUser().getIdcode())){
                GetUserType newUser = new GetUserType();
                LoginToken newUserToken = new LoginToken();
                newUserToken.setUserName(parameter.getUsername());
                newUserToken.setPassWord(parameter.getPassword());
                newUserToken.setUserId(BigInteger.valueOf(nextUserId++));
                newUserToken.setToken(UUID.randomUUID().toString());
                newUser.setId(newUserToken.getUserId());
                newUser.setName(parameter.getUser().getName());
                newUser.setUsername(parameter.getUsername());
                newUser.setPhoneNumber(parameter.getUser().getPhoneNumber());
                newUser.setIdcode(parameter.getUser().getIdcode());
                newUser.setEmail(parameter.getUser().getEmail());
                newUser.setBirthDate(parameter.getUser().getBirthDate());
                newUser.setAddress(parameter.getUser().getAddress());
                users.add(newUser);
                tokens.add(newUserToken);
                response.setSuccess(SuccessType.TRUE);
                response.setUser(newUser);
            } else if(!methodStack.checkUsernameFree(parameter.getUsername())) {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UsrNErr");
                error.setErrorMessage("Username already in use!");
                response.setError(error);
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-IdCdErr");
                error.setErrorMessage("Id code already in use!");
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public AddInstitutionResponse addInstitution(AddInstitutionRequest parameter) {
        //DONE
        AddInstitutionResponse response = new AddInstitutionResponse();
        try {
            if(methodStack.authorizeToken(parameter.getToken())){
                if(methodStack.checkInstitutionRegistryCodeFree(parameter.getInstitution().getRegistryCode())){
                    InstitutionType newInstitution;
                    newInstitution = parameter.getInstitution();
                    institutions.add(newInstitution);
                    InstitutionMemberType institutionManager = new InstitutionMemberType();
                    institutionManager.setId(methodStack.getUserIdByToken(parameter.getToken()));
                    institutionManager.setRegistryCode(newInstitution.getRegistryCode());
                    institutionManager.setMemberType("manager");
                    institutionManagers.add(institutionManager);
                    institutionManager.setMemberType("member");
                    institutionMembers.add(institutionManager);
                    response.setSuccess(SuccessType.TRUE);
                    response.setInstitution(newInstitution);
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-regcodeErr");
                    error.setErrorMessage("Registry code already in use!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public AddGroupResponse addGroup(AddGroupRequest parameter) {
        //DONE
        AddGroupResponse response = new AddGroupResponse();
        try {
            if(methodStack.getTokenAuthorized(parameter.getToken())){
                if(methodStack.validateInstitutionManager(parameter.getToken(), parameter.getRegistryCode())){
                    if(!methodStack.getGroupRegistred(parameter.getGroup().getName(), parameter.getRegistryCode())){
                        GetGroupType newGroup = new GetGroupType();
                        InstitutionGroupType institutionGroup = new InstitutionGroupType();
                        newGroup.setId(BigInteger.valueOf(nextGroupId++));
                        newGroup.setName(parameter.getGroup().getName());
                        newGroup.setType(parameter.getGroup().getType());
                        newGroup.setLocation(parameter.getGroup().getLocation());
                        newGroup.setDescription(parameter.getGroup().getDescription());
                        groups.add(newGroup);
                        institutionGroup.setGroupId(newGroup.getId());
                        institutionGroup.setRegistryCode(parameter.getRegistryCode());
                        institutionGroups.add(institutionGroup);
                        response.setSuccess(SuccessType.TRUE);
                        response.setGroup(newGroup);
                    } else {
                        response.setSuccess(SuccessType.FALSE);
                        error.setErrorCode("4xx-GrErr");
                        error.setErrorMessage("Group name already in use in institution!");
                        response.setError(error);
                    }
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-UaErr");
                    error.setErrorMessage("Token not permitted to add group to institution!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public AddInstitutionManagerResponse addInstitutionManager(AddInstitutionManagerRequest parameter) {
        //DONE
        AddInstitutionManagerResponse response = new AddInstitutionManagerResponse();
        try {
            if(methodStack.getTokenAuthorized(parameter.getToken())){
                if(methodStack.authorizeInstitutionManager(parameter.getToken(), parameter.getManager().getRegistryCode())){
                    if(!methodStack.getAlreadyManager(parameter.getManager())){
                        InstitutionMemberType manager = parameter.getManager();
                        manager.setMemberType("manager");
                        institutionManagers.add(manager);
                        manager.setMemberType("member");
                        if(!methodStack.getAlreadyMember(manager)){
                            institutionMembers.add(manager);
                        }
                        response.setSuccess(SuccessType.TRUE);
                        response.setUser(methodStack.getUserByUserId(parameter.getManager().getId()));
                    } else {
                        response.setSuccess(SuccessType.FALSE);
                        error.setErrorCode("4xx-IManErr");
                        error.setErrorMessage("User already institution manager");
                        response.setError(error);
                    }
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-UaErr");
                    error.setErrorMessage("Token not permitted to add institution manager!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("400xUaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public AddInstitutionMemberResponse addInstitutionMember(AddInstitutionMemberRequest parameter) {
        //DONE
        AddInstitutionMemberResponse response = new AddInstitutionMemberResponse();
        try {
            if(methodStack.getTokenAuthorized(parameter.getToken())){
                if(methodStack.authorizeInstitutionManager(parameter.getToken(), parameter.getMember().getRegistryCode())){
                    if(!methodStack.getAlreadyMember(parameter.getMember())){
                        InstitutionMemberType manager = parameter.getMember();
                        manager.setMemberType("member");
                        institutionMembers.add(manager);
                        response.setSuccess(SuccessType.TRUE);
                        response.setUser(methodStack.getUserByUserId(parameter.getMember().getId()));
                    } else {
                        response.setSuccess(SuccessType.FALSE);
                        error.setErrorCode("4xx-IMemErr");
                        error.setErrorMessage("User already institution member!");
                        response.setError(error);
                    }
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-UaErr");
                    error.setErrorMessage("Token not permitted to add institution member!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public AddGroupManagerResponse addGroupManager(AddGroupManagerRequest parameter) {
        //DONE
        AddGroupManagerResponse response = new AddGroupManagerResponse();
        try {
            if(methodStack.getTokenAuthorized(parameter.getToken())){
                if(methodStack.authorizeInstitutionManager(parameter.getToken(), methodStack.getInstitutionWithGroupId(parameter.getManager().getGroupId()).getRegistryCode())){
                    if((methodStack.getGroupWithId(parameter.getManager().getGroupId())!=null) && 
                            (methodStack.authorizeInstitutionMember(parameter.getManager().getId(), methodStack.getInstitutionWithGroupId(parameter.getManager().getGroupId()).getRegistryCode()) &&
                            !(methodStack.authorizeGroupManager(parameter.getManager().getId(), parameter.getManager().getGroupId())))){
                        GroupMemberType manager = parameter.getManager();
                        manager.setMemberType("manager");
                        groupManagers.add(manager);
                        response.setSuccess(SuccessType.TRUE);
                        response.setUser(methodStack.getUserWithId(parameter.getManager().getId()));
                    } else {
                        response.setSuccess(SuccessType.FALSE);
                        error.setErrorCode("4xx-NotMemErr");
                        error.setErrorMessage("User is not member of intitution or is already group manager!");
                        response.setError(error);
                    }
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-UaErr");
                    error.setErrorMessage("Token not permitted to add group manager!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public AddGroupMemberResponse addGroupMember(AddGroupMemberRequest parameter) {
        //DONE
        AddGroupMemberResponse response = new AddGroupMemberResponse();
        try {
            if(methodStack.getTokenAuthorized(parameter.getToken())){
                if(!methodStack.authorizeGroupMember(parameter.getMember().getId(), parameter.getMember().getGroupId())){
                    GroupMemberType member = parameter.getMember();
                    member.setMemberType("member");
                    groupMembers.add(member);
                    response.setSuccess(SuccessType.TRUE);
                    response.setUser(methodStack.getUserWithId(parameter.getMember().getId()));
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-GrMemErr");
                    error.setErrorMessage("User already group member!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public GetUserResponse getUser(GetUserRequest parameter) {
        //DONE
        GetUserResponse response = new GetUserResponse();
        try {
            UserListType use = new UserListType();
            if(methodStack.getTokenAuthorized(parameter.getToken())){
                if(parameter.getId()!=null){
                    if(methodStack.getUserWithId(parameter.getId())!=null){
                        use.getUser().add(methodStack.getUserWithId(parameter.getId()));
                        response.setUsers(use);
                        response.setSuccess(SuccessType.TRUE);
                    } else {
                        response.setSuccess(SuccessType.FALSE);
                        error.setErrorCode("4xx-UsrReqErr");
                        error.setErrorMessage("Inserted id matches no user!");
                        response.setError(error);
                    }
                } else {
                    for(GetUserType user:users){
                        use.getUser().add(user);
                    }
                    if(use.getUser().isEmpty()){
                        response.setSuccess(SuccessType.FALSE);
                        error.setErrorCode("4xx-UsrReqErr");
                        error.setErrorMessage("No users with to show!");
                        response.setError(error);
                    } else {
                        response.setUsers(use);
                        response.setSuccess(SuccessType.TRUE);
                    }
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("400xUaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public GetInstitutionResponse getInstitution(GetInstitutionRequest parameter) {
        //DONE
        GetInstitutionResponse response = new GetInstitutionResponse();
        try {
            if(methodStack.getTokenAuthorized(parameter.getToken())){
                InstitutionListType intsitutionList = new InstitutionListType();
                if(parameter.getRegistryCode()!=null){
                    for(InstitutionType inst:institutions){
                        if(inst.getRegistryCode().equalsIgnoreCase(parameter.getRegistryCode())){
                            GetInstitutionType getInst = new GetInstitutionType();
                            GroupListType groupList = new GroupListType();
                            getInst.setInfo(inst);
                            for(GetGroupType group:groups){
                                for(InstitutionGroupType instGroup:institutionGroups){
                                    if(group.getId().equals(instGroup.getGroupId()) && inst.getRegistryCode().equalsIgnoreCase(instGroup.getRegistryCode())){
                                        groupList.getGroup().add(group);
                                    }
                                }
                            }
                            getInst.setGroupList(groupList);
                            intsitutionList.getInstitution().add(getInst);
                        }
                    }
                    if(intsitutionList.getInstitution().isEmpty()){
                        error.setErrorCode("4xx-InfErr");
                        error.setErrorMessage("Inserted registry code matches no institution!");
                        response.setError(error);
                    } else {
                        response.setSuccess(SuccessType.TRUE);
                        response.setInstitutionList(intsitutionList);
                    }
                } else {
                    for(InstitutionType inst:institutions){
                        GetInstitutionType getInst = new GetInstitutionType();
                        GroupListType groupList = new GroupListType();
                        getInst.setInfo(inst);
                        for(GetGroupType group:groups){
                            for(InstitutionGroupType instGroup:institutionGroups){
                                if(group.getId().equals(instGroup.getGroupId()) && inst.getRegistryCode().equalsIgnoreCase(instGroup.getRegistryCode())){
                                    groupList.getGroup().add(group);
                                }
                            }
                        }
                        getInst.setGroupList(groupList);
                        intsitutionList.getInstitution().add(getInst);
                    }
                    if(intsitutionList.getInstitution().isEmpty()){
                        error.setErrorCode("4xx-InfErr");
                        error.setErrorMessage("No institutions to show!");
                        response.setError(error);
                    } else {
                        response.setSuccess(SuccessType.TRUE);
                        response.setInstitutionList(intsitutionList);
                    }
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public GetGroupResponse getGroup(GetGroupRequest parameter) {
        //DONE
        GetGroupResponse response = new GetGroupResponse();
        try {
            if(methodStack.getTokenAuthorized(parameter.getToken())){
                GroupListType groups = new GroupListType();
                if(parameter.getGroupId()!=null){
                    for(InstitutionGroupType institutionGroup:institutionGroups){
                        if(institutionGroup.getRegistryCode().equalsIgnoreCase(parameter.getRegistryCode()) && institutionGroup.getGroupId().equals(parameter.getGroupId())){
                            groups.getGroup().add(methodStack.getGroupWithId(parameter.getGroupId()));
                        }
                    }
                } else {
                    for(InstitutionGroupType institutionGroup:institutionGroups){
                        if(institutionGroup.getRegistryCode().equalsIgnoreCase(parameter.getRegistryCode())){
                            groups.getGroup().add(methodStack.getGroupWithId(institutionGroup.getGroupId()));
                        }
                    }
                }
                if(!groups.getGroup().isEmpty()){
                    response.setSuccess(SuccessType.TRUE);
                    response.setGroupList(groups);
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-GrpErr");
                    error.setErrorMessage("No groups to show!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public GetInstitutionManagerResponse getInstitutionManager(GetInstitutionManagerRequest parameter) {
        //TODO implement this method
        GetInstitutionManagerResponse response = new GetInstitutionManagerResponse();
        try {
            UserListType managers = new UserListType();
            if(methodStack.getTokenAuthorized(parameter.getToken())){
                if(parameter.getId()!=null){
                    for(InstitutionMemberType manager:institutionManagers){
                        if(manager.getId().equals(parameter.getId()) && manager.getRegistryCode().equalsIgnoreCase(parameter.getRegistryCode())){
                            managers.getUser().add(methodStack.getUserWithId(parameter.getId()));
                        }
                    }
                } else {
                    for(InstitutionMemberType manager:institutionManagers){
                        if(manager.getRegistryCode().equalsIgnoreCase(parameter.getRegistryCode())){
                            managers.getUser().add(methodStack.getUserWithId(manager.getId()));
                        }
                    }
                }
                if(managers.getUser().isEmpty()){
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-NManErr");
                    error.setErrorMessage("No managers to show!");
                    response.setError(error);
                } else {
                    response.setSuccess(SuccessType.TRUE);
                    response.setManagerList(managers);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public GetInstitutionMemberResponse getInstitutionMember(GetInstitutionMemberRequest parameter) {
        //DONE
        GetInstitutionMemberResponse response = new GetInstitutionMemberResponse();
        try {
            UserListType members = new UserListType();
            if(methodStack.getTokenAuthorized(parameter.getToken())){
                if(parameter.getId()!=null){
                    for(InstitutionMemberType member:institutionMembers){
                        if(member.getId().equals(parameter.getId()) && member.getRegistryCode().equalsIgnoreCase(parameter.getRegistryCode())){
                            members.getUser().add(methodStack.getUserWithId(parameter.getId()));
                        }
                    }
                } else {
                    for(InstitutionMemberType member:institutionMembers){
                        if(member.getRegistryCode().equalsIgnoreCase(parameter.getRegistryCode())){
                            members.getUser().add(methodStack.getUserWithId(member.getId()));
                        }
                    }
                }
                if(members.getUser().isEmpty()){
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-NMemErr");
                    error.setErrorMessage("No members to show!");
                    response.setError(error);
                } else {
                    response.setSuccess(SuccessType.TRUE);
                    response.setUserList(members);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public GetGroupManagerResponse getGroupManager(GetGroupManagerRequest parameter) {
        //TODO implement this method
        GetGroupManagerResponse response = new GetGroupManagerResponse();
        try {
            UserListType managers = new UserListType();
            if(methodStack.getTokenAuthorized(parameter.getToken())){
                if(parameter.getId()!=null){
                    for(GroupMemberType manager:groupManagers){
                        if(manager.getId().equals(parameter.getId()) && manager.getGroupId().equals(parameter.getGroupId())){
                            managers.getUser().add(methodStack.getUserWithId(parameter.getId()));
                        }
                    }
                } else {
                    for(GroupMemberType manager:groupManagers){
                        if(manager.getGroupId().equals(parameter.getGroupId())){
                            managers.getUser().add(methodStack.getUserWithId(manager.getId()));
                        }
                    }
                }
                if(managers.getUser().isEmpty()){
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-NManErr");
                    error.setErrorMessage("No managers to show!");
                    response.setError(error);
                } else {
                    response.setSuccess(SuccessType.TRUE);
                    response.setUserList(managers);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public GetGroupMemberResponse getGroupMember(GetGroupMemberRequest parameter) {
        //TODO implement this method
        GetGroupMemberResponse response = new GetGroupMemberResponse();
        try {
            UserListType members = new UserListType();
            if(methodStack.getTokenAuthorized(parameter.getToken())){
                if(parameter.getId()!=null){
                    for(GroupMemberType member:groupMembers){
                        if(member.getId().equals(parameter.getId()) && member.getGroupId().equals(parameter.getGroupId())){
                            members.getUser().add(methodStack.getUserWithId(parameter.getId()));
                        }
                    }
                } else {
                    for(GroupMemberType member:groupMembers){
                        if(member.getGroupId().equals(parameter.getGroupId())){
                            members.getUser().add(methodStack.getUserWithId(member.getId()));
                        }
                    }
                }
                if(members.getUser().isEmpty()){
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-NMemErr");
                    error.setErrorMessage("No members to show!");
                    response.setError(error);
                } else {
                    response.setSuccess(SuccessType.TRUE);
                    response.setUserList(members);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public DeleteUserResponse deleteUser(DeleteUserRequest parameter) {
        //DONE
        DeleteUserResponse response = new DeleteUserResponse();
        try {
            if(methodStack.authorizeToken(parameter.getToken()) && methodStack.getUserIdByToken(parameter.getToken()).equals(parameter.getId())){
                if(methodStack.authorizeUserDeletion(parameter.getId())){
                    List<InstitutionMemberType> newMembers = new ArrayList<InstitutionMemberType>();
                    List<InstitutionMemberType> newManagers = new ArrayList<InstitutionMemberType>();
                    List<GroupMemberType> groupMemb = new ArrayList<GroupMemberType>();
                    List<GroupMemberType> groupMana = new ArrayList<GroupMemberType>();
                    List<GetUserType> usrs = new ArrayList<>();
                    List<LoginToken> tkns = new ArrayList<>();
                    GetUserType deleteUser = null;
                    for(InstitutionMemberType manager:institutionManagers){
                        if(!manager.getId().equals(parameter.getId())){
                            newManagers.add(manager);
                        }
                    }
                    for(InstitutionMemberType member:institutionMembers){
                        if(!member.getId().equals(parameter.getId())){
                            newMembers.add(member);
                        }
                    }
                    for(GroupMemberType manager:groupManagers){
                        if(manager.getId().equals(parameter.getId())){
                            groupMana.add(manager);
                        }
                    }
                    for(GroupMemberType member:groupMembers){
                        if(member.getId().equals(parameter.getId())){
                            groupMemb.add(member);
                        }
                    }
                    for(GetUserType user:users){
                        if(user.getId().equals(parameter.getId())){
                            response.setUser(user);
                            response.setSuccess(SuccessType.TRUE);
                            //users.remove(user);
                            deleteUser = user;
                        } else {
                            usrs.add(user);
                        }
                    }
                    for(LoginToken token:tokens){
                        if(token.getToken().equals(parameter.getToken()) && token.getUserId().equals(parameter.getId())){
                            //tokens.remove(token);
                        } else {
                            tkns.add(token);
                        }
                    }
                    if(deleteUser != null){
                        groupManagers.clear();
                        groupMembers.clear();
                        institutionManagers.clear();
                        institutionMembers.clear();
                        users.clear();
                        tokens.clear();
                        groupManagers.addAll(groupMana);
                        groupMembers.addAll(groupMemb);
                        institutionManagers.addAll(newMembers);
                        institutionMembers.addAll(newMembers);
                        users.addAll(usrs);
                        tokens.addAll(tkns);
                    } else {
                        response.setSuccess(SuccessType.FALSE);
                        error.setErrorCode("4xx-UsrNFErr");
                        error.setErrorMessage("Inserted id matches no user to delete!");
                        response.setError(error);
                    }
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-UDErr");
                    error.setErrorMessage("Cannot delete user who is only manager to one or more institutions!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage("Inserted token is not allowed to delete user!");
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public DeleteInstitutionResponse deleteInstitution(DeleteInstitutionRequest parameter) {
        //DONE
        DeleteInstitutionResponse response = new DeleteInstitutionResponse();
        try {
            if(methodStack.authorizeToken(parameter.getToken())){
                if(methodStack.authorizeInstitutionManager(methodStack.getUserWithToken(parameter.getToken()).getId(), parameter.getRegistryCode())){
                    response.setSuccess(SuccessType.TRUE);
                    response.setInstitution(methodStack.getInstitutionByRegistryCode(parameter.getRegistryCode()));
                    List<InstitutionMemberType> newMembers = new ArrayList<InstitutionMemberType>();
                    List<InstitutionMemberType> newManagers = new ArrayList<InstitutionMemberType>();
                    List<InstitutionGroupType> newGroups = new ArrayList<InstitutionGroupType>();
                    List<GetGroupType> gro = new ArrayList<>();
                    List<GroupMemberType> groupMemb = new ArrayList<GroupMemberType>();
                    List<GroupMemberType> groupMana = new ArrayList<GroupMemberType>();
                    List<InstitutionType> newInstitutions = new ArrayList<InstitutionType>();
                    InstitutionType institute = null;
                    for(InstitutionGroupType institutionGroup:institutionGroups){
                        if(!institutionGroup.getRegistryCode().equalsIgnoreCase(parameter.getRegistryCode())){
                            for(GroupMemberType manager:groupManagers){
                                if(manager.getGroupId().equals(institutionGroup.getGroupId())){
                                    groupMana.add(manager);
                                }
                            }
                            for(GroupMemberType member:groupMembers){
                                if(member.getGroupId().equals(institutionGroup.getGroupId())){
                                    groupMemb.add(member);
                                }
                            }
                            //institutionGroups.remove(institutionGroup);
                            newGroups.add(institutionGroup);
                            gro.add(methodStack.getGroupWithId(institutionGroup.getGroupId()));
                        }
                    }
                    for(InstitutionMemberType member:institutionMembers){
                        if(!member.getRegistryCode().equalsIgnoreCase(parameter.getRegistryCode())){
                            newMembers.add(member);
                        }
                    }
                    for(InstitutionMemberType manager:institutionManagers){
                        if(!manager.getRegistryCode().equalsIgnoreCase(parameter.getRegistryCode())){
                            newManagers.add(manager);
                        }
                    }
                    for(InstitutionType institution:institutions){
                        if(!institution.getRegistryCode().equals(parameter.getRegistryCode())){
                            newInstitutions.add(institution);
                        } else {
                            institute = institution;
                        }
                    }
                    if(institute!=null){
                        groupManagers.clear();
                        groupMembers.clear();
                        groups.clear();
                        groups.addAll(gro);
                        institutionGroups.clear();
                        institutionGroups.addAll(newGroups);
                        institutionMembers.clear();
                        institutionMembers.addAll(newMembers);
                        institutionManagers.clear();
                        institutionManagers.addAll(newManagers);
                        institutions.clear();
                        institutions.addAll(newInstitutions);
                        response.setInstitution(institute);
                        response.setSuccess(SuccessType.TRUE);
                    } else {
                        response.setSuccess(SuccessType.FALSE);
                        error.setErrorCode("4xx-InstErr");
                        error.setErrorMessage("Inserted registry code matches no institution to delete!");
                        response.setError(error);
                    }
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-UaErr");
                    error.setErrorMessage("Inserted token is not allowed to delete institution!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public DeleteGroupResponse deleteGroup(DeleteGroupRequest parameter) {
        //DONE
        DeleteGroupResponse response = new DeleteGroupResponse();
        try {
            if(methodStack.authorizeToken(parameter.getToken())){
                if(methodStack.authorizeInstitutionManager(methodStack.getUserWithToken(parameter.getToken()).getId(), methodStack.getInstitutionWithGroupId(parameter.getGroupId()).getRegistryCode())){
                    List<GetGroupType> grps = new ArrayList<>();
                    List<GroupMemberType> newManagers = new ArrayList<>();
                    List<GroupMemberType> newMembers = new ArrayList<>();
                    GetGroupType grp = null;
                    for(GetGroupType group:groups){
                        if(!group.getId().equals(parameter.getGroupId())){
                            grps.add(group);
                            for(GroupMemberType member:groupMembers){
                                if(member.getGroupId().equals(parameter.getGroupId())){
                                    newMembers.add(member);
                                }
                            }
                            for(GroupMemberType manager:groupManagers){
                                if(manager.getGroupId().equals(parameter.getGroupId())){
                                    newManagers.add(manager);
                                }
                            }
                            //groups.remove(group);
                            //response.setSuccess(SuccessType.TRUE);
                        } else {
                            grp = group;
                        }
                    }
                    if(grp != null){
                        groups.clear();
                        groups.addAll(grps);
                        groupManagers.clear();
                        groupMembers.clear();
                        groupManagers.addAll(newManagers);
                        groupMembers.addAll(newMembers);
                        response.setGroup(grp);
                        response.setSuccess(SuccessType.TRUE);
                    } else {
                        response.setSuccess(SuccessType.FALSE);
                        error.setErrorCode("4xx-GDErr");
                        error.setErrorMessage("Inserted id matches no group to delete!");
                        response.setError(error);
                    }
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-UaErr");
                    error.setErrorMessage("Inserted token is not allowed to delete group!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public DeleteInstitutionManagerResponse deleteInstitutionManager(DeleteInstitutionManagerRequest parameter) {
        //DONE
        DeleteInstitutionManagerResponse response = new DeleteInstitutionManagerResponse();
        try {
            if(methodStack.authorizeToken(parameter.getToken())){
                if(methodStack.authorizeInstitutionManager(methodStack.getUserWithToken(parameter.getToken()).getId(), parameter.getManager().getRegistryCode())){
                    if(!methodStack.checkOnlyManager(parameter.getManager().getRegistryCode())){
                        InstitutionMemberType toRemove = null;
                        List<InstitutionMemberType> newManagers = new ArrayList<InstitutionMemberType>();
                        for(InstitutionMemberType manager:institutionManagers){
                            if(manager.getId().equals(parameter.getManager().getId()) && manager.getRegistryCode().equalsIgnoreCase(parameter.getManager().getRegistryCode())){
                                toRemove = manager;
                            } else {
                                newManagers.add(manager);
                            }
                        }
                        institutionManagers.clear();
                        institutionManagers.addAll(newManagers);
                        response.setSuccess(SuccessType.TRUE);
                        response.setUser(methodStack.getUserWithId(toRemove.getId()));
                    } else {
                        response.setSuccess(SuccessType.FALSE);
                        error.setErrorCode("4xx-ManMisErr");
                        error.setErrorMessage("Institution cannot be without a manager! Assign substitute manager before deletion!");
                        response.setError(error);
                    }
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-UaErr");
                    error.setErrorMessage("Inserted token is not allowed to delete manager!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public DeleteInstitutionMemberResponse deleteInstitutionMember(DeleteInstitutionMemberRequest parameter) {
        //TODO implement this method
        DeleteInstitutionMemberResponse response = new DeleteInstitutionMemberResponse();
        try {
            if(methodStack.authorizeToken(parameter.getToken())){
                if(methodStack.authorizeInstitutionManager(methodStack.getUserWithToken(parameter.getToken()).getId(), parameter.getMember().getRegistryCode())){
                    if((methodStack.authorizeInstitutionManager(parameter.getMember().getId(), parameter.getMember().getRegistryCode()) &&
                            !methodStack.checkOnlyManager(parameter.getMember().getRegistryCode())) || 
                            (!methodStack.authorizeInstitutionManager(parameter.getMember().getId(), parameter.getMember().getRegistryCode()))){
                        List<InstitutionMemberType> newMembers = new ArrayList<InstitutionMemberType>();
                        List<InstitutionMemberType> newManagers = new ArrayList<InstitutionMemberType>();
                        InstitutionMemberType memb = null;
                        for(InstitutionMemberType manager:institutionManagers){
                            if(manager.getId().equals(parameter.getMember().getId()) && manager.getRegistryCode().equalsIgnoreCase(parameter.getMember().getRegistryCode())){
                                //institutionManagers.remove(manager);
                            } else {
                                newManagers.add(manager);
                            }
                        }
                        for(InstitutionMemberType member:institutionMembers){
                            if(member.getId().equals(parameter.getMember().getId()) && member.getRegistryCode().equalsIgnoreCase(parameter.getMember().getRegistryCode())){
                                memb = member;
                            } else {
                                newMembers.add(member);
                            }
                        }
                        institutionManagers.clear();
                        institutionMembers.clear();
                        institutionManagers.addAll(newManagers);
                        institutionMembers.addAll(newMembers);
                        response.setSuccess(SuccessType.TRUE);
                        response.setUser(methodStack.getUserWithId(memb.getId()));
                    } else {
                        response.setSuccess(SuccessType.FALSE);
                        error.setErrorCode("4xx-ManMisErr");
                        error.setErrorMessage("Institution cannot be without a manager! Assign substitute manager before institution member deletion!");
                        response.setError(error);
                    }
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-UaErr");
                    error.setErrorMessage("Inserted token is not allowed to delete member!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400xUaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public DeleteGroupManagerResponse deleteGroupManager(DeleteGroupManagerRequest parameter) {
        //DONE
        DeleteGroupManagerResponse response = new DeleteGroupManagerResponse();
        try {
            if(methodStack.authorizeToken(parameter.getToken())){
                if(methodStack.authorizeInstitutionManager(parameter.getToken(), methodStack.getInstitutionWithGroupId(parameter.getManager().getGroupId()).getRegistryCode())){
                    GroupMemberType mana = null;
                    List<GroupMemberType> newManagers = new ArrayList<GroupMemberType>();
                    for(GroupMemberType manager:groupManagers){
                        if(manager.getId().equals(parameter.getManager().getId()) && manager.getGroupId().equals(parameter.getManager().getGroupId())){
                            //response.setUser(methodStack.getUserWithId(parameter.getManager().getId()));
                            //response.setSuccess(SuccessType.TRUE);
                            //groupManagers.remove(manager);
                            mana = manager;
                        } else {
                            newManagers.add(manager);
                        }
                    }
                    if(mana!=null){
                        groupManagers.clear();
                        groupManagers.addAll(newManagers);
                        response.setUser(methodStack.getUserWithId(mana.getId()));
                        response.setSuccess(SuccessType.TRUE);
                    } else {
                        response.setSuccess(SuccessType.FALSE);
                        error.setErrorCode("4xx-EmptyActionErr");
                        error.setErrorMessage("User is not group manager!");
                        response.setError(error);
                    }
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-UaManErr");
                    error.setErrorMessage("Inserted token not permitted to delete group manager!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage(methodStack.errorList.get("400UaErr"));
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public DeleteGroupMemberResponse deleteGroupMember(DeleteGroupMemberRequest parameter) {
        //DONE
        DeleteGroupMemberResponse response = new DeleteGroupMemberResponse();
        try {
            if(methodStack.authorizeToken(parameter.getToken())){
                if(methodStack.authorizeInstitutionManager(parameter.getToken(), methodStack.getInstitutionWithGroupId(parameter.getMember().getGroupId()).getRegistryCode())){
                    GroupMemberType memb = null;
                    List<GroupMemberType> newMembers = new ArrayList<GroupMemberType>();
                    for(GroupMemberType member:groupMembers){
                        if(member.getId().equals(parameter.getMember().getId()) && member.getGroupId().equals(parameter.getMember().getGroupId())){
                            memb = member;
                        } else {
                            newMembers.add(member);
                        }
                    }
                    if(memb!=null){
                        response.setUser(methodStack.getUserWithId(memb.getId()));
                        response.setSuccess(SuccessType.TRUE);
                        groupMembers.clear();
                        groupMembers.addAll(newMembers);
                    } else {
                        response.setSuccess(SuccessType.FALSE);
                        error.setErrorCode("4xx-EmptyActionErr");
                        error.setErrorMessage("User is not group member!");
                        response.setError(error);
                    }
                } else if(methodStack.getUserWithToken(parameter.getToken()).getId().equals(parameter.getMember().getId())){
                    GroupMemberType memb = null;
                    List<GroupMemberType> newMembers = new ArrayList<GroupMemberType>();
                    for(GroupMemberType member:groupMembers){
                        if(member.getId().equals(parameter.getMember().getId()) && member.getGroupId().equals(parameter.getMember().getGroupId())){
                            memb = member;
                        } else {
                            newMembers.add(member);
                        }
                    }
                    if(memb!=null){
                        response.setUser(methodStack.getUserWithId(memb.getId()));
                        response.setSuccess(SuccessType.TRUE);
                        groupMembers.clear();
                        groupMembers.addAll(newMembers);
                    } else {
                        response.setSuccess(SuccessType.FALSE);
                        error.setErrorCode("4xx-EmptyActionErr");
                        error.setErrorMessage("User is not group member!");
                        response.setError(error);
                    }
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-UaErr");
                    error.setErrorMessage("Inserted token not permitted to delete group members!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage("Process with token unauthorized!");
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public UpdateUserResponse updateUser(UpdateUserRequest parameter) {
        //DONE
        UpdateUserResponse response = new UpdateUserResponse();
        try {
            if(methodStack.getUserIdByToken(parameter.getToken()).equals(parameter.getId())){
                GetUserType newUser = null;
                for(GetUserType user:users){
                    if(user.getId().equals(parameter.getId()) && user.getIdcode().equalsIgnoreCase(parameter.getUser().getIdcode())){
                        newUser = user;
                        newUser.setAddress(parameter.getUser().getAddress());
                        newUser.setBirthDate(parameter.getUser().getBirthDate());
                        newUser.setEmail(parameter.getUser().getEmail());
                        newUser.setName(parameter.getUser().getName());
                        newUser.setPhoneNumber(parameter.getUser().getPhoneNumber());
                        response.setSuccess(SuccessType.TRUE);
                        response.setUser(newUser);
                        users.set(users.indexOf(user),newUser);
                    }
                }
                if(newUser == null){
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-UaErr");
                    error.setErrorMessage("Inserted values are not valid for user with identification code!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage("Process with token unauthorized!");
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public UpdateInstitutionResponse updateInstitution(UpdateInstitutionRequest parameter) {
        //DONE
        UpdateInstitutionResponse response = new UpdateInstitutionResponse();
        try {
            if(methodStack.authorizeToken(parameter.getToken())){
                if(methodStack.authorizeInstitutionManager(parameter.getToken(), parameter.getInstitution().getRegistryCode())){
                    for(InstitutionType institution:institutions){
                        if(institution.getRegistryCode().equalsIgnoreCase(parameter.getInstitution().getRegistryCode())){
                            InstitutionType newInstitution = institution;
                            newInstitution.setAddress(parameter.getInstitution().getAddress());
                            newInstitution.setEmail(parameter.getInstitution().getEmail());
                            newInstitution.setName(parameter.getInstitution().getName());
                            newInstitution.setPhoneNumber(parameter.getInstitution().getPhoneNumber());
                            newInstitution.setType(parameter.getInstitution().getType());
                            newInstitution.setWebAddress(parameter.getInstitution().getWebAddress());
                            institutions.set(institutions.indexOf(institution),newInstitution);
                            response.setInstitution(newInstitution);
                            response.setSuccess(SuccessType.TRUE);
                        }
                    }
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-UaErr");
                    error.setErrorMessage("Institution update is not allowed with inserted token!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage("Process with token unauthorized!");
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }

    public UpdateGroupResponse updateGroup(UpdateGroupRequest parameter) {
        //DONE
        UpdateGroupResponse response = new UpdateGroupResponse();
        try {
            if(methodStack.authorizeToken(parameter.getToken())){
                if(methodStack.authorizeGroupManager(parameter.getToken(), parameter.getGroupId()) || 
                    methodStack.authorizeInstitutionManager(parameter.getToken(), methodStack.getInstitutionWithGroupId(parameter.getGroupId()).getRegistryCode())){
                    GetGroupType newGroup = null;
                    for(GetGroupType group:groups){
                        if(group.getId().equals(parameter.getGroupId())){
                            newGroup = group;
                            if(methodStack.checkGroupUnregistred(methodStack.getInstitutionWithGroupId(parameter.getGroupId()).getRegistryCode(), parameter.getGroup().getName())){
                                newGroup.setName(parameter.getGroup().getName());
                                newGroup.setDescription(parameter.getGroup().getDescription());
                                newGroup.setLocation(parameter.getGroup().getLocation());
                                newGroup.setType(parameter.getGroup().getType());
                                groups.set(groups.indexOf(group),newGroup);
                                response.setGroup(newGroup);
                                response.setSuccess(SuccessType.TRUE);
                            }
                        }
                    }
                    if(newGroup == null){
                        response.setSuccess(SuccessType.FALSE);
                        error.setErrorCode("4xx-GrRegErr");
                        error.setErrorMessage("Inserted group name already registered in institution!");
                        response.setError(error);
                    }
                } else {
                    response.setSuccess(SuccessType.FALSE);
                    error.setErrorCode("4xx-UaErr");
                    error.setErrorMessage("Group update is not allowed with inserted token!");
                    response.setError(error);
                }
            } else {
                response.setSuccess(SuccessType.FALSE);
                error.setErrorCode("4xx-UaErr");
                error.setErrorMessage("Process with token unauthorized!");
                response.setError(error);
            }
        } catch (WebApplicationException e){
            
        }
        return response;
    }
}