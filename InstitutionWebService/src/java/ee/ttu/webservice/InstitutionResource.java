/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.webservice;

import ee.ttu.webservice.institution.*;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Sander Puntso
 */
@Path("institution")
public class InstitutionResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of InstitutionResource
     */
    public InstitutionResource() {
    }
    
    InstitutionService ws = new InstitutionService();

    /**
     * Retrieves representation of an instance of ee.ttu.webservice.InstitutionResource
     * @return an instance of ee.ttu.webservice.institution.InstitutionType
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public AddInstitutionResponse addInstitution(@QueryParam("token") String token,
            InstitutionType content) {
        //TODO return proper representation object
        AddInstitutionRequest request = new AddInstitutionRequest();
        request.setToken(token);
        request.setInstitution(content);
        return ws.addInstitution(request);
    }
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public GetInstitutionResponse getInstitution(@QueryParam("token") String token) {
        //TODO return proper representation object
        GetInstitutionRequest request = new GetInstitutionRequest();
        request.setToken(token);
        return ws.getInstitution(request);
    }
    @GET
    @Path("/{registryCode}")
    @Produces(MediaType.APPLICATION_JSON)
    public GetInstitutionResponse getInstitution(@QueryParam("token") String token,
            @PathParam("registryCode") String registryCode) {
        //TODO return proper representation object
        GetInstitutionRequest request = new GetInstitutionRequest();
        request.setToken(token);
        request.setRegistryCode(registryCode);
        return ws.getInstitution(request);
    }
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public UpdateInstitutionResponse updateInstitution(@QueryParam("token") String token,
            InstitutionType content) {
        //TODO return proper representation object
        UpdateInstitutionRequest request = new UpdateInstitutionRequest();
        request.setToken(token);
        request.setInstitution(content);
        return ws.updateInstitution(request);
    }
    @DELETE
    @Path("/{registryCode}")
    @Produces(MediaType.APPLICATION_JSON)
    public DeleteInstitutionResponse deleteInstitution(@QueryParam("token") String token, @PathParam("registryCode") String registryCode) {
        //TODO return proper representation object
        DeleteInstitutionRequest request = new DeleteInstitutionRequest();
        request.setToken(token);
        request.setRegistryCode(registryCode);
        return ws.deleteInstitution(request);
    }

    /**
     * PUT method for updating or creating an instance of InstitutionResource
     * @param content representation for the resource
     */
    @POST
    @Path("/{registryCode}/group")
    @Produces(MediaType.APPLICATION_JSON)
    public AddGroupResponse addGroup(@PathParam("registryCode") String registryCode,
            @QueryParam("token") String token,
            GroupType content) {
        AddGroupRequest request = new AddGroupRequest();
        request.setRegistryCode(registryCode);
        request.setToken(token);
        request.setGroup(content);
        return ws.addGroup(request);
    }
    @GET
    @Path("/{registryCode}/group")
    @Produces(MediaType.APPLICATION_JSON)
    public GetGroupResponse getGroup(@PathParam("registryCode") String registryCode,
            @QueryParam("token") String token) {
        GetGroupRequest request = new GetGroupRequest();
        request.setRegistryCode(registryCode);
        request.setToken(token);
        return ws.getGroup(request);
    }
    @GET
    @Path("/{registryCode}/group/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public GetGroupResponse getGroup(@PathParam("registryCode") String registryCode,
            @QueryParam("token") String token,
            @PathParam("id") BigInteger id) {
        GetGroupRequest request = new GetGroupRequest();
        request.setRegistryCode(registryCode);
        request.setToken(token);
        request.setGroupId(id);
        return ws.getGroup(request);
    }
    @PUT
    @Path("/group/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UpdateGroupResponse updateGroup(@QueryParam("token") String token,
            @PathParam("id") BigInteger id,
            GroupType content) {
        UpdateGroupRequest request = new UpdateGroupRequest();
        request.setToken(token);
        request.setGroupId(id);
        request.setGroup(content);
        return ws.updateGroup(request);
    }
    @DELETE
    @Path("/group/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public DeleteGroupResponse deleteGroup(@QueryParam("token") String token,
            @PathParam("id") BigInteger id) {
        DeleteGroupRequest request = new DeleteGroupRequest();
        request.setToken(token);
        request.setGroupId(id);
        return ws.deleteGroup(request);
    }
    
    @POST
    @Path("/{registryCode}/member/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public AddInstitutionMemberResponse addInstitutionMember(@PathParam("registryCode") String registryCode,
            @QueryParam("token") String token,
            @PathParam("id") BigInteger id) {
        AddInstitutionMemberRequest request = new AddInstitutionMemberRequest();
        request.setToken(token);
        InstitutionMemberType member = new InstitutionMemberType();
        member.setRegistryCode(registryCode);
        member.setId(id);
        member.setMemberType("member");
        request.setMember(member);
        return ws.addInstitutionMember(request);
    }
    @GET
    @Path("/{registryCode}/member/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public GetInstitutionMemberResponse getInstitutionMember(@PathParam("registryCode") String registryCode,
            @QueryParam("token") String token,
            @PathParam("id") BigInteger id) {
        GetInstitutionMemberRequest request = new GetInstitutionMemberRequest();
        request.setToken(token);
        request.setId(id);
        request.setRegistryCode(registryCode);
        return ws.getInstitutionMember(request);
    }
    @GET
    @Path("/{registryCode}/member")
    @Produces(MediaType.APPLICATION_JSON)
    public GetInstitutionMemberResponse getInstitutionMember(@PathParam("registryCode") String registryCode,
            @QueryParam("token") String token) {
        GetInstitutionMemberRequest request = new GetInstitutionMemberRequest();
        request.setToken(token);
        request.setRegistryCode(registryCode);
        return ws.getInstitutionMember(request);
    }
    @DELETE
    @Path("/{registryCode}/member/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public DeleteInstitutionMemberResponse deleteInstitutionMember(@PathParam("registryCode") String registryCode,
            @QueryParam("token") String token,
            @PathParam("id") BigInteger id) {
        DeleteInstitutionMemberRequest request = new DeleteInstitutionMemberRequest();
        request.setToken(token);
        InstitutionMemberType member = new InstitutionMemberType();
        member.setRegistryCode(registryCode);
        member.setId(id);
        member.setMemberType("member");
        request.setMember(member);
        return ws.deleteInstitutionMember(request);
    }
    
    @POST
    @Path("/{registryCode}/manager/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public AddInstitutionManagerResponse addInstitutionManager(@PathParam("registryCode") String registryCode,
            @QueryParam("token") String token,
            @PathParam("id") BigInteger id) {
        AddInstitutionManagerRequest request = new AddInstitutionManagerRequest();
        request.setToken(token);
        InstitutionMemberType manager = new InstitutionMemberType();
        manager.setRegistryCode(registryCode);
        manager.setId(id);
        manager.setMemberType("manager");
        request.setManager(manager);
        return ws.addInstitutionManager(request);
    }
    @GET
    @Path("/{registryCode}/manager/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public GetInstitutionManagerResponse getInstitutionManager(@PathParam("registryCode") String registryCode,
            @QueryParam("token") String token,
            @PathParam("id") BigInteger id) {
        GetInstitutionManagerRequest request = new GetInstitutionManagerRequest();
        request.setToken(token);
        request.setId(id);
        request.setRegistryCode(registryCode);
        return ws.getInstitutionManager(request);
    }
    @GET
    @Path("/{registryCode}/manager")
    @Produces(MediaType.APPLICATION_JSON)
    public GetInstitutionManagerResponse getInstitutionManager(@PathParam("registryCode") String registryCode,
            @QueryParam("token") String token) {
        GetInstitutionManagerRequest request = new GetInstitutionManagerRequest();
        request.setToken(token);
        request.setRegistryCode(registryCode);
        return ws.getInstitutionManager(request);
    }
    @DELETE
    @Path("/{registryCode}/manager/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public DeleteInstitutionManagerResponse deleteInstitutionManager(@PathParam("registryCode") String registryCode,
            @QueryParam("token") String token,
            @PathParam("id") BigInteger id) {
        DeleteInstitutionManagerRequest request = new DeleteInstitutionManagerRequest();
        request.setToken(token);
        InstitutionMemberType member = new InstitutionMemberType();
        member.setRegistryCode(registryCode);
        member.setId(id);
        member.setMemberType("manager");
        request.setManager(member);
        return ws.deleteInstitutionManager(request);
    }
    
    @POST
    @Path("/group/{groupId}/manager/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public AddGroupManagerResponse addGroupManager(@PathParam("groupId") BigInteger groupId,
            @QueryParam("token") String token,
            @PathParam("id") BigInteger id) {
        AddGroupManagerRequest request = new AddGroupManagerRequest();
        request.setToken(token);
        GroupMemberType manager = new GroupMemberType();
        manager.setGroupId(groupId);
        manager.setId(id);
        manager.setMemberType("manager");
        request.setManager(manager);
        return ws.addGroupManager(request);
    }
    @GET
    @Path("/group/{groupId}/manager/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public GetGroupManagerResponse getGroupManager(@PathParam("groupId") BigInteger groupId,
            @QueryParam("token") String token,
            @PathParam("id") BigInteger id) {
        GetGroupManagerRequest request = new GetGroupManagerRequest();
        request.setToken(token);
        request.setId(id);
        request.setGroupId(groupId);
        return ws.getGroupManager(request);
    }
    @GET
    @Path("/group/{groupId}/manager")
    @Produces(MediaType.APPLICATION_JSON)
    public GetGroupManagerResponse getGroupManager(@PathParam("groupId") BigInteger groupId,
            @QueryParam("token") String token) {
        GetGroupManagerRequest request = new GetGroupManagerRequest();
        request.setToken(token);
        request.setGroupId(groupId);
        return ws.getGroupManager(request);
    }
    @DELETE
    @Path("/group/{groupId}/manager/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public DeleteGroupManagerResponse deleteGroupManager(@PathParam("groupId") BigInteger groupId,
            @QueryParam("token") String token,
            @PathParam("id") BigInteger id) {
        DeleteGroupManagerRequest request = new DeleteGroupManagerRequest();
        request.setToken(token);
        GroupMemberType manager = new GroupMemberType();
        manager.setGroupId(groupId);
        manager.setId(id);
        manager.setMemberType("manager");
        request.setManager(manager);
        return ws.deleteGroupManager(request);
    }
    
    @POST
    @Path("/group/{groupId}/member/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public AddGroupMemberResponse addGroupMember(@PathParam("groupId") BigInteger groupId,
            @QueryParam("token") String token,
            @PathParam("id") BigInteger id) {
        AddGroupMemberRequest request = new AddGroupMemberRequest();
        request.setToken(token);
        GroupMemberType member = new GroupMemberType();
        member.setGroupId(groupId);
        member.setId(id);
        member.setMemberType("member");
        request.setMember(member);
        return ws.addGroupMember(request);
    }
    @GET
    @Path("/group/{groupId}/member/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public GetGroupMemberResponse getGroupMember(@PathParam("groupId") BigInteger groupId,
            @QueryParam("token") String token,
            @PathParam("id") BigInteger id) {
        GetGroupMemberRequest request = new GetGroupMemberRequest();
        request.setToken(token);
        request.setId(id);
        request.setGroupId(groupId);
        return ws.getGroupMember(request);
    }
    @GET
    @Path("/group/{groupId}/member")
    @Produces(MediaType.APPLICATION_JSON)
    public GetGroupMemberResponse getGroupMember(@PathParam("groupId") BigInteger groupId,
            @QueryParam("token") String token) {
        GetGroupMemberRequest request = new GetGroupMemberRequest();
        request.setToken(token);
        request.setGroupId(groupId);
        return ws.getGroupMember(request);
    }
    @DELETE
    @Path("/group/{groupId}/member/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public DeleteGroupMemberResponse deleteGroupMember(@PathParam("groupId") BigInteger groupId,
            @QueryParam("token") String token,
            @PathParam("id") BigInteger id) {
        DeleteGroupMemberRequest request = new DeleteGroupMemberRequest();
        request.setToken(token);
        GroupMemberType member = new GroupMemberType();
        member.setGroupId(groupId);
        member.setId(id);
        member.setMemberType("member");
        request.setMember(member);
        return ws.deleteGroupMember(request);
    }
}